const mongoose = require('mongoose');

const {Schema} = mongoose;

const dummySchema = new Schema({
    first_name: String,
    second_name:String
});

mongoose.model('Dummy', dummySchema);