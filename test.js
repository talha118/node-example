
/**Rabbitmq example */

// const amqp = require('amqplib');
//
// async function init() {
//     try {
//         const conn = await amqp.connect('amqp://localhost');
//         const channel = await conn.createChannel();
//         return Promise.resolve(channel);
//
//     } catch(error){
//         return Promise.reject(error);
//     }
// }
// init()
//     .then(channel=>{
//         // console.log('Connected');
//         channel.assertQueue('ig_gw.send_message.8', {durable: false});
//         channel.assertQueue('mc.send_message.8', {durable: false});
//
//         const message = {};
//         message['thread_id'] = "340282366841710300949128235256610498266";
//         message['message'] = {};
//         message['message']['type'] = 'photo';
//         message['message']['url'] = 'https://test-s3.directheroes.com/minio/images/img_snowtops.jpg';
//
//         channel.sendToQueue('ig_gw.send_message.8', new Buffer(JSON.stringify(message)));
//
//         channel.consume('mc.send_message.8', (msg) => {
//             console.log(JSON.parse(msg.content.toString('utf8')));
//         });
//     }).catch(error=>console.log(error));


/** Mongo Example*/

const mongoose = require('mongoose');

require('./Models/Dummy');

const Dummy = mongoose.model('Dummy');

mongoose.promise = global.promise;

mongoose.connect('mongodb://192.168.100.252/dummy');
mongoose.set('debug', true);

const newDummy = new Dummy({first_name: "Muhammad", second_name: "Talha"});
newDummy.save().then((data)=>console.log(data));